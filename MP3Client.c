/*********************************************************************
 *
 *	Shoutcast MP3/WAV Client
 *  Module for Microchip TCP/IP Stack
 *	 -Downloads and plays MP3 audio streams from the Internet
 *
 *********************************************************************
 * FileName:        MP3Client.c
 * Dependencies:    Microchip TCP/IP Stack, SPISRAM2.c
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F
 * Complier:        Microchip C18 v3.10 or higher
 *					Microchip C30 v2.05 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright � 2002-2007 Microchip Technology Inc.  All rights 
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and 
 * distribute: 
 * (i)  the Software when embedded on a Microchip microcontroller or 
 *      digital signal controller product (�Device�) which is 
 *      integrated into Licensee�s product; or
 * (ii) ONLY the Software driver source files ENC28J60.c and 
 *      ENC28J60.h ported to a non-Microchip device used in 
 *      conjunction with a Microchip ethernet controller for the 
 *      sole purpose of interfacing with the ethernet controller. 
 *
 * You should refer to the license agreement accompanying this 
 * Software for additional information regarding your rights and 
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT 
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT 
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL 
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR 
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF 
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS 
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE 
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER 
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT 
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Francesco Volpe		12/??/2006	Original
 * Howard Schlunder		03/12/2007	Completely revamped
 ********************************************************************/
#include "TCPIP.h"
#include "MP3Client.h"
#include "SPIRAM2.h"
#include "OLED.h"
#include "VLSICodec.h"


#define MP3_INTERRUPT_FREQ	1800ul	// Hz

//#define MP3_DEBUG_PRINT_BUFFER_FILLED

// FIFO for MP3 data (in external SPI RAM chip)
// Note that you can shrink this length of move the start address 
// if you want to use part of the RAM chip for something else
#define AUDIO_BUFFER_START	(0ul)
#define AUDIO_BUFFER_LEN	(32768ul)
#if defined (__18CXX)
    #define WaitForDataByte()   {while(!MP3_SPI_IF); MP3_SPI_IF = 0;}
    #define SPI_ON_BIT          (MP3_SPICON1bits.SSPEN)

#define NTP_SERVER "10.0.10.200"
#define NTP_SERVER_PORT (8000ul)

static volatile WORD wHeadPtr = AUDIO_BUFFER_START; 
static volatile WORD wTailPtr = AUDIO_BUFFER_START;
WORD wHeadPtrShadow=AUDIO_BUFFER_START;
WORD wTailPtrShadow=AUDIO_BUFFER_START;


extern BOOL playf;
BOOL play_streaming=1;
BOOL Play_IR=0;
static BOOL Openned = FALSE;
//TCP_SOCKET MySocket = INVALID_SOCKET;
static UDP_SOCKET MySocket = INVALID_UDP_SOCKET;

BYTE vBuffer[32];
static BOOL bUnderrunOccured = TRUE;
int z=0;



static enum
	{
		SM_HOME = 0,
		SM_UDP_IS_OPENED,
        SM_CLEAN_BUFFER,
        SM_PLAYING
	} smMP3State = SM_HOME;


// Pseudo Functions
#define LOW(a) 		((a) & 0xFF)
#define HIGH(a) 	(((a)>>8) & 0xFF)


/*********************************************************************
* Function:		void MP3ClientInit(void)
*
* PreCondition:	None
*
* Input:		None
*
* Output:		None
*
* Side Effects:	None
*
* Overview:		Sets up needed interrupts for the MP3 decoder 
*				(TMR3 for SPI idle detection and feeding MP3 decoder)
*
* Note:			None
********************************************************************/


void MP3ClientInit(void)
{
    #if defined( __18CXX )
        // Set up Timer 3 to automatically feed the MP3 decoder periodically
        T3CON = 0xB1;   // 16-bit mode, 1:8 prescale, internal clock, timer enabled
        TMR3H = HIGH(-((GetPeripheralClock()/8 + MP3_INTERRUPT_FREQ/2)/MP3_INTERRUPT_FREQ));
        TMR3L = LOW(-((GetPeripheralClock()/8 + MP3_INTERRUPT_FREQ/2)/MP3_INTERRUPT_FREQ));
        IPR2bits.TMR3IP = 1;    // High priority interrupt
        PIR2bits.TMR3IF = 0;    // Clear interrupt flag
        PIE2bits.TMR3IE = 1;    // Enable the interrupt
   
}


/*********************************************************************
* Function:		void MP3ClientTask(void)
*
* PreCondition:	None
*
* Input:		None
*
* Output:		None
*
* Side Effects:	None
*
* Overview:		Downloads an MP3 or WAV from a Shoutcast server or 
*				similar and plays it.
*
* Note:			None
********************************************************************/
void MP3ClientTask(void)
{

	WORD w, wSpace;
	BYTE i, j;
    static DWORD playTimer;
	//WORD wHeadPtrShadow=AUDIO_BUFFER_START;
	//WORD wTailPtrShadow=AUDIO_BUFFER_START;

	// Reconnect if we lost our connection to the remote server -- a 
	// common task that needs to be done in almost every state
	

	// Get buffer head and tail shadows (interrupt safe)

        	
        
        //MAQUINA DE ESTADO DEL CLIENTE MP3


        PIE2bits.TMR3IE = 0;
        wHeadPtrShadow = wHeadPtr;
        wTailPtrShadow = wTailPtr;
        PIE2bits.TMR3IE = 1;
        
	switch(smMP3State)
	{
		
	
		// Wait for connection to be established
		case SM_HOME:
			if(MySocket == INVALID_UDP_SOCKET)
				MySocket = UDPOpenEx(0,UDP_OPEN_SERVER,NTP_SERVER_PORT,NTP_SERVER_PORT);
                            //MySocket =UDPOpenEx((DWORD)NTP_SERVER,UDP_OPEN_ROM_HOST,0,NTP_SERVER_PORT);
                                // UDPOpenEx(SERVER,UDP_OPEN_RAM_HOST,NTP_SERVER_PORT,NTP_SERVER_PORT);
			smMP3State++;
			break;

		case SM_UDP_IS_OPENED:
			if(UDPIsOpened(MySocket) == TRUE)
			{
				smMP3State = SM_CLEAN_BUFFER;
			}
                        
        case SM_CLEAN_BUFFER:                
            if(UDPIsGetReady(MySocket)){
                STAND_BY_O = 1;//prendido
                #ifdef DOBLE_AUDIO
                STAND_BY_2_O = 1;//prendido 2
                #endif
                playf=1;                
                play_streaming = 0;
                bUnderrunOccured = TRUE; 
                playTimer = TickGet();
                smMP3State = SM_PLAYING;
            
            for (i = 0; i < sizeof (vBuffer); i++) {
                vBuffer[i] = 0;
            }
            wHeadPtrShadow = AUDIO_BUFFER_START;
            wTailPtrShadow = AUDIO_BUFFER_START;
            wHeadPtr = wHeadPtrShadow;
            wTailPtr = wTailPtrShadow;
//            UDPDiscard();
                break;
            }
        break;
            
		case SM_PLAYING:
           
			while(play_streaming==0){
				// Calculate the free space in our ring buffer           
				if(wHeadPtrShadow >= wTailPtrShadow)
					wSpace = (AUDIO_BUFFER_LEN - 1) - (wHeadPtrShadow - wTailPtrShadow);
				else
					wSpace = wTailPtrShadow - wHeadPtrShadow - 1;

				if(wSpace == 0 )
                                    break;

				// Find the number of bytes waiting in the TCP FIFO

				w = UDPIsGetReady(MySocket);
				if(w == 0 )
                    break; 
                
                playTimer = TickGet();
                if(w > wSpace)
                    w = wSpace;
                if(w > sizeof(vBuffer))
                    w = sizeof(vBuffer);
                if(w >= AUDIO_BUFFER_START + AUDIO_BUFFER_LEN - wHeadPtrShadow){
                    w = AUDIO_BUFFER_START + AUDIO_BUFFER_LEN - wHeadPtrShadow;
                    UDPGetArray(vBuffer, w);
                    SPIRAM2PutArray(wHeadPtrShadow, vBuffer, w);
                    wHeadPtrShadow = AUDIO_BUFFER_START;                                           
                } 
                else{
                    UDPGetArray(vBuffer, w);        
                    SPIRAM2PutArray(wHeadPtrShadow, vBuffer, w);        
                    wHeadPtrShadow += w;      
                    }	
                }
			break;
	}
    
    if(TickGet()-playTimer > 2*TICK_SECOND && smMP3State == SM_PLAYING){
        playf=0;
        play_streaming = 1;
        STAND_BY_O = 0;//apagado
        #ifdef DOBLE_AUDIO
        STAND_BY_2_O = 0;//apagado 2
        #endif
        smMP3State = SM_CLEAN_BUFFER;
        wHeadPtr = AUDIO_BUFFER_START; 
        wTailPtr = AUDIO_BUFFER_START;
        wHeadPtrShadow=AUDIO_BUFFER_START;
        wTailPtrShadow=AUDIO_BUFFER_START;
        VLSI_Process_Stop();
    }
        
    
        PIE2bits.TMR3IE = 0;
        wHeadPtr = wHeadPtrShadow;
        PIE2bits.TMR3IE = 1;
       
        
        
       

}

/*********************************************************************
* Function:		void MP3OpenStation(STATION_INFO *lpStation)
*
* PreCondition:	None
*
* Input:		*lpStation: STATION_INFO filled will ip address, 
*				port, and message
*
* Output:		None
*
* Side Effects:	None
*
* Overview:		Begins connected to the remote station
*
* Note:			None
********************************************************************/



/*********************************************************************
* Function:		void MP3CloseStation(void)
*
* PreCondition:	None
*
* Input:		None
*
* Output:		None
*
* Side Effects:	None
*
* Overview:		Stops playing the current station, and disconnects 
*				if any.
*
* Note:			None
********************************************************************/
void MP3CloseStation(void)
{
	Openned = FALSE;

	if(MySocket != INVALID_UDP_SOCKET)
	{
		UDPClose(MySocket);
		MySocket = INVALID_UDP_SOCKET;
		smMP3State = SM_HOME;
	}
}


/*********************************************************************
* Function:		void MP3Interrupt(void)
*
* PreCondition:	None
*
* Input:		None
*
* Output:		None
*
* Side Effects:	None
*
* Overview:		Periodically copies data into the VS1011 MP3 decoder 
*				chip when the DREQ signal (INT0) is asserted (active 
*				high)
*
* Note:			This is meant for the interrupt context only.  Do 
*				not call from main line code.
********************************************************************/
#pragma tmpdata MP3ISRTemp
void MP3Timer3Interrupt(void)
{
	//static BYTE vBuffer[64];
	static WORD wSpace;
	static BYTE i, j;
	
//	// Reset Timer 3 so that we can poll for a time when the SPI is free
//	TMR3H = HIGH(-((INSTR_FREQ/8 + MP3_INTERRUPT_FREQ/2)/MP3_INTERRUPT_FREQ));
//	TMR3L = LOW(-((INSTR_FREQ/8 + MP3_INTERRUPT_FREQ/2)/MP3_INTERRUPT_FREQ));
//
//	// Clear Timer 3 intrrupt flag
//	PIR2bits.TMR3IF = 0;
        
                // Reset Timer 3 so that we can poll for a time when the SPI is free
        TMR3H = HIGH(-((GetPeripheralClock()/8 + MP3_INTERRUPT_FREQ/2)/MP3_INTERRUPT_FREQ));
        TMR3L = LOW(-((GetPeripheralClock()/8 + MP3_INTERRUPT_FREQ/2)/MP3_INTERRUPT_FREQ));

        // Clear Timer 3 intrrupt flag
        PIR2bits.TMR3IF = 0;
	// Return immediately if there is nothing to do
        
	if(!MP3_DREQ_IO)
		return;

	// Return immediately if we can't process this interrupt 
	// because something else is using the SPI right now
	if((SPIRAM_CS_IO == 0) || (SPIRAM2_CS_IO == 0) || (MP3_XCS_IO == 0))
		return;

	// Ensure we have at least 32 bytes in the ring buffer to copy
	if(wHeadPtr >= wTailPtr)
		wSpace = wHeadPtr - wTailPtr;
	else
		wSpace = AUDIO_BUFFER_LEN - (wTailPtr - wHeadPtr);
	
	/*if(wSpace < 32)
	{
		bUnderrunOccured = TRUE;
		return;
	}
	*/
	// After an underrun condition, do not start playing again 
	// until we have at least half a second of buffered data

        
        if(bUnderrunOccured)
            {
                    if(wSpace < 8192)
                        return;
                    //bUnderrunOccured = FALSE;
            }
        
        
	// Read 32 bytes from the audio buffer
	SPIRAM2GetArray(wTailPtr, vBuffer, sizeof(vBuffer));
	wTailPtr += sizeof(vBuffer);
	if(wTailPtr >= AUDIO_BUFFER_START + AUDIO_BUFFER_LEN)
		wTailPtr = AUDIO_BUFFER_START;

	// Write the 32 bytes to the MP3 decoder
        
            MP3_XDCS_IO = 0;
            // Set the data CS pin low
            for(i = 0; i < sizeof(vBuffer); i++)
            {
		PIR1bits.SSP1IF = 0;
                //WriteSPI(vBuffer[i]);
		SSP1BUF = vBuffer[i];
                //while(!PIR1bits.SSP1IF);
                Delayus(8);
                //PIR1bits.SSP1IF

		
                
            }
            MP3_XDCS_IO = 1;                  // Set the data CS pin high
                 


}
#pragma tmpdata
