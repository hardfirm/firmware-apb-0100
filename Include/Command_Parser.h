/* 
 * File:   Command_Parser.h
 * Author: Fernando Marotta
 *
 * Created on 6 de octubre de 2018, 21:36
 */


#define MAX_COMMANDS_RX 10
#define MAX_LENGTH_COMMAND 17

#ifndef COMMAND_PARSER_H
#define	COMMAND_PARSER_H
#include "TCPIP.h"
#include "TCP.h"


void Command_Parser_Parser_Command(TCP_SOCKET MySocket2, char * TramaValida, int indice);
void Command_Parser_Restore_Saved_State( void );
/********************* Api Structure With Fucntions Pointers ******************/
/*
typedef struct _API_COMMAND_PARSER {
void ( *Parser_Command ) ( TCP_SOCKET MySocket2, char* TramaValida, int indice);
void ( *Restore_Saved_State ) ( void );
} const API_COMMAND_PARSER;

extern API_COMMAND_PARSER Command_Parser;
 */
#endif	/* COMMAND_PARSER_H */

