/* 
 * File:   Dotacion_Movil.h
 * Author: Fernando Marotta
 *
 * Created on 7 de octubre de 2018, 00:48
 */

#ifndef DOTACION_MOVIL_H
#define	DOTACION_MOVIL_H
#include "p18f97j60.h"



void Display_Dotacion(unsigned int num);
void Display_Movil(unsigned int num);

/********************* Api Structure With Fucntions Pointers ******************/
/*
typedef struct _API_DISPLAY_7SEG {
void ( *Dotacion ) ( int num );
void ( *Movil )    ( int num );
}const API_DISPLAY_7SEG;

extern API_DISPLAY_7SEG Display;
*/

#endif	/* DOTACION_MOVIL_H */

