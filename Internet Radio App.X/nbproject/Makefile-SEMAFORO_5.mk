#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-SEMAFORO_5.mk)" "nbproject/Makefile-local-SEMAFORO_5.mk"
include nbproject/Makefile-local-SEMAFORO_5.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=SEMAFORO_5
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Internet_Radio_App.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Internet_Radio_App.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../Flash/EraseFlash.c ../Flash/ReadFlash.c ../Flash/WriteBlockFlash.c ../Flash/WriteBytesFlash.c ../Flash/WriteWordFlash.c ../TCPIP_Stack/ARP.c ../TCPIP_Stack/Announce.c ../TCPIP_Stack/DHCP.c ../TCPIP_Stack/DNS.c ../TCPIP_Stack/ETH97J60.c ../TCPIP_Stack/HTTP2.c ../TCPIP_Stack/Helpers.c ../TCPIP_Stack/ICMP.c ../TCPIP_Stack/IP.c ../TCPIP_Stack/MPFS2.c ../TCPIP_Stack/NBNS.c ../TCPIP_Stack/Reboot.c ../TCPIP_Stack/SPIRAM.c ../TCPIP_Stack/StackTsk.c ../TCPIP_Stack/TCP.c ../TCPIP_Stack/Tick.c ../TCPIP_Stack/UDP.c ../MainDemo.c ../SPIRAM2.c ../MP3Client.c ../VLSICodec.c ../GenericTCPClient.c ../Command_Parser.c ../Leds_Pwm.c ../Dotacion_Movil.c ../Command_Execution.c ../CustomHTTPApp.c ../IOConfiguration.c ../InputHandler.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/2146075967/EraseFlash.o ${OBJECTDIR}/_ext/2146075967/ReadFlash.o ${OBJECTDIR}/_ext/2146075967/WriteBlockFlash.o ${OBJECTDIR}/_ext/2146075967/WriteBytesFlash.o ${OBJECTDIR}/_ext/2146075967/WriteWordFlash.o ${OBJECTDIR}/_ext/927647166/ARP.o ${OBJECTDIR}/_ext/927647166/Announce.o ${OBJECTDIR}/_ext/927647166/DHCP.o ${OBJECTDIR}/_ext/927647166/DNS.o ${OBJECTDIR}/_ext/927647166/ETH97J60.o ${OBJECTDIR}/_ext/927647166/HTTP2.o ${OBJECTDIR}/_ext/927647166/Helpers.o ${OBJECTDIR}/_ext/927647166/ICMP.o ${OBJECTDIR}/_ext/927647166/IP.o ${OBJECTDIR}/_ext/927647166/MPFS2.o ${OBJECTDIR}/_ext/927647166/NBNS.o ${OBJECTDIR}/_ext/927647166/Reboot.o ${OBJECTDIR}/_ext/927647166/SPIRAM.o ${OBJECTDIR}/_ext/927647166/StackTsk.o ${OBJECTDIR}/_ext/927647166/TCP.o ${OBJECTDIR}/_ext/927647166/Tick.o ${OBJECTDIR}/_ext/927647166/UDP.o ${OBJECTDIR}/_ext/1472/MainDemo.o ${OBJECTDIR}/_ext/1472/SPIRAM2.o ${OBJECTDIR}/_ext/1472/MP3Client.o ${OBJECTDIR}/_ext/1472/VLSICodec.o ${OBJECTDIR}/_ext/1472/GenericTCPClient.o ${OBJECTDIR}/_ext/1472/Command_Parser.o ${OBJECTDIR}/_ext/1472/Leds_Pwm.o ${OBJECTDIR}/_ext/1472/Dotacion_Movil.o ${OBJECTDIR}/_ext/1472/Command_Execution.o ${OBJECTDIR}/_ext/1472/CustomHTTPApp.o ${OBJECTDIR}/_ext/1472/IOConfiguration.o ${OBJECTDIR}/_ext/1472/InputHandler.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/2146075967/EraseFlash.o.d ${OBJECTDIR}/_ext/2146075967/ReadFlash.o.d ${OBJECTDIR}/_ext/2146075967/WriteBlockFlash.o.d ${OBJECTDIR}/_ext/2146075967/WriteBytesFlash.o.d ${OBJECTDIR}/_ext/2146075967/WriteWordFlash.o.d ${OBJECTDIR}/_ext/927647166/ARP.o.d ${OBJECTDIR}/_ext/927647166/Announce.o.d ${OBJECTDIR}/_ext/927647166/DHCP.o.d ${OBJECTDIR}/_ext/927647166/DNS.o.d ${OBJECTDIR}/_ext/927647166/ETH97J60.o.d ${OBJECTDIR}/_ext/927647166/HTTP2.o.d ${OBJECTDIR}/_ext/927647166/Helpers.o.d ${OBJECTDIR}/_ext/927647166/ICMP.o.d ${OBJECTDIR}/_ext/927647166/IP.o.d ${OBJECTDIR}/_ext/927647166/MPFS2.o.d ${OBJECTDIR}/_ext/927647166/NBNS.o.d ${OBJECTDIR}/_ext/927647166/Reboot.o.d ${OBJECTDIR}/_ext/927647166/SPIRAM.o.d ${OBJECTDIR}/_ext/927647166/StackTsk.o.d ${OBJECTDIR}/_ext/927647166/TCP.o.d ${OBJECTDIR}/_ext/927647166/Tick.o.d ${OBJECTDIR}/_ext/927647166/UDP.o.d ${OBJECTDIR}/_ext/1472/MainDemo.o.d ${OBJECTDIR}/_ext/1472/SPIRAM2.o.d ${OBJECTDIR}/_ext/1472/MP3Client.o.d ${OBJECTDIR}/_ext/1472/VLSICodec.o.d ${OBJECTDIR}/_ext/1472/GenericTCPClient.o.d ${OBJECTDIR}/_ext/1472/Command_Parser.o.d ${OBJECTDIR}/_ext/1472/Leds_Pwm.o.d ${OBJECTDIR}/_ext/1472/Dotacion_Movil.o.d ${OBJECTDIR}/_ext/1472/Command_Execution.o.d ${OBJECTDIR}/_ext/1472/CustomHTTPApp.o.d ${OBJECTDIR}/_ext/1472/IOConfiguration.o.d ${OBJECTDIR}/_ext/1472/InputHandler.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/2146075967/EraseFlash.o ${OBJECTDIR}/_ext/2146075967/ReadFlash.o ${OBJECTDIR}/_ext/2146075967/WriteBlockFlash.o ${OBJECTDIR}/_ext/2146075967/WriteBytesFlash.o ${OBJECTDIR}/_ext/2146075967/WriteWordFlash.o ${OBJECTDIR}/_ext/927647166/ARP.o ${OBJECTDIR}/_ext/927647166/Announce.o ${OBJECTDIR}/_ext/927647166/DHCP.o ${OBJECTDIR}/_ext/927647166/DNS.o ${OBJECTDIR}/_ext/927647166/ETH97J60.o ${OBJECTDIR}/_ext/927647166/HTTP2.o ${OBJECTDIR}/_ext/927647166/Helpers.o ${OBJECTDIR}/_ext/927647166/ICMP.o ${OBJECTDIR}/_ext/927647166/IP.o ${OBJECTDIR}/_ext/927647166/MPFS2.o ${OBJECTDIR}/_ext/927647166/NBNS.o ${OBJECTDIR}/_ext/927647166/Reboot.o ${OBJECTDIR}/_ext/927647166/SPIRAM.o ${OBJECTDIR}/_ext/927647166/StackTsk.o ${OBJECTDIR}/_ext/927647166/TCP.o ${OBJECTDIR}/_ext/927647166/Tick.o ${OBJECTDIR}/_ext/927647166/UDP.o ${OBJECTDIR}/_ext/1472/MainDemo.o ${OBJECTDIR}/_ext/1472/SPIRAM2.o ${OBJECTDIR}/_ext/1472/MP3Client.o ${OBJECTDIR}/_ext/1472/VLSICodec.o ${OBJECTDIR}/_ext/1472/GenericTCPClient.o ${OBJECTDIR}/_ext/1472/Command_Parser.o ${OBJECTDIR}/_ext/1472/Leds_Pwm.o ${OBJECTDIR}/_ext/1472/Dotacion_Movil.o ${OBJECTDIR}/_ext/1472/Command_Execution.o ${OBJECTDIR}/_ext/1472/CustomHTTPApp.o ${OBJECTDIR}/_ext/1472/IOConfiguration.o ${OBJECTDIR}/_ext/1472/InputHandler.o

# Source Files
SOURCEFILES=../Flash/EraseFlash.c ../Flash/ReadFlash.c ../Flash/WriteBlockFlash.c ../Flash/WriteBytesFlash.c ../Flash/WriteWordFlash.c ../TCPIP_Stack/ARP.c ../TCPIP_Stack/Announce.c ../TCPIP_Stack/DHCP.c ../TCPIP_Stack/DNS.c ../TCPIP_Stack/ETH97J60.c ../TCPIP_Stack/HTTP2.c ../TCPIP_Stack/Helpers.c ../TCPIP_Stack/ICMP.c ../TCPIP_Stack/IP.c ../TCPIP_Stack/MPFS2.c ../TCPIP_Stack/NBNS.c ../TCPIP_Stack/Reboot.c ../TCPIP_Stack/SPIRAM.c ../TCPIP_Stack/StackTsk.c ../TCPIP_Stack/TCP.c ../TCPIP_Stack/Tick.c ../TCPIP_Stack/UDP.c ../MainDemo.c ../SPIRAM2.c ../MP3Client.c ../VLSICodec.c ../GenericTCPClient.c ../Command_Parser.c ../Leds_Pwm.c ../Dotacion_Movil.c ../Command_Execution.c ../CustomHTTPApp.c ../IOConfiguration.c ../InputHandler.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-SEMAFORO_5.mk dist/${CND_CONF}/${IMAGE_TYPE}/Internet_Radio_App.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F97J60
MP_PROCESSOR_OPTION_LD=18f97j60
MP_LINKER_DEBUG_OPTION=  -u_DEBUGSTACK
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/2146075967/EraseFlash.o: ../Flash/EraseFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2146075967" 
	@${RM} ${OBJECTDIR}/_ext/2146075967/EraseFlash.o.d 
	@${RM} ${OBJECTDIR}/_ext/2146075967/EraseFlash.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/2146075967/EraseFlash.o   ../Flash/EraseFlash.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2146075967/EraseFlash.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2146075967/EraseFlash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2146075967/ReadFlash.o: ../Flash/ReadFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2146075967" 
	@${RM} ${OBJECTDIR}/_ext/2146075967/ReadFlash.o.d 
	@${RM} ${OBJECTDIR}/_ext/2146075967/ReadFlash.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/2146075967/ReadFlash.o   ../Flash/ReadFlash.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2146075967/ReadFlash.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2146075967/ReadFlash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2146075967/WriteBlockFlash.o: ../Flash/WriteBlockFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2146075967" 
	@${RM} ${OBJECTDIR}/_ext/2146075967/WriteBlockFlash.o.d 
	@${RM} ${OBJECTDIR}/_ext/2146075967/WriteBlockFlash.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/2146075967/WriteBlockFlash.o   ../Flash/WriteBlockFlash.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2146075967/WriteBlockFlash.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2146075967/WriteBlockFlash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2146075967/WriteBytesFlash.o: ../Flash/WriteBytesFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2146075967" 
	@${RM} ${OBJECTDIR}/_ext/2146075967/WriteBytesFlash.o.d 
	@${RM} ${OBJECTDIR}/_ext/2146075967/WriteBytesFlash.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/2146075967/WriteBytesFlash.o   ../Flash/WriteBytesFlash.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2146075967/WriteBytesFlash.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2146075967/WriteBytesFlash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2146075967/WriteWordFlash.o: ../Flash/WriteWordFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2146075967" 
	@${RM} ${OBJECTDIR}/_ext/2146075967/WriteWordFlash.o.d 
	@${RM} ${OBJECTDIR}/_ext/2146075967/WriteWordFlash.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/2146075967/WriteWordFlash.o   ../Flash/WriteWordFlash.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2146075967/WriteWordFlash.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2146075967/WriteWordFlash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/ARP.o: ../TCPIP_Stack/ARP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/ARP.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/ARP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/ARP.o   ../TCPIP_Stack/ARP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/ARP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/ARP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/Announce.o: ../TCPIP_Stack/Announce.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/Announce.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/Announce.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/Announce.o   ../TCPIP_Stack/Announce.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/Announce.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/Announce.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/DHCP.o: ../TCPIP_Stack/DHCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/DHCP.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/DHCP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/DHCP.o   ../TCPIP_Stack/DHCP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/DHCP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/DHCP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/DNS.o: ../TCPIP_Stack/DNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/DNS.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/DNS.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/DNS.o   ../TCPIP_Stack/DNS.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/DNS.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/DNS.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/ETH97J60.o: ../TCPIP_Stack/ETH97J60.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/ETH97J60.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/ETH97J60.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/ETH97J60.o   ../TCPIP_Stack/ETH97J60.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/ETH97J60.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/ETH97J60.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/HTTP2.o: ../TCPIP_Stack/HTTP2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/HTTP2.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/HTTP2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/HTTP2.o   ../TCPIP_Stack/HTTP2.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/HTTP2.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/HTTP2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/Helpers.o: ../TCPIP_Stack/Helpers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/Helpers.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/Helpers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/Helpers.o   ../TCPIP_Stack/Helpers.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/Helpers.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/Helpers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/ICMP.o: ../TCPIP_Stack/ICMP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/ICMP.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/ICMP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/ICMP.o   ../TCPIP_Stack/ICMP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/ICMP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/ICMP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/IP.o: ../TCPIP_Stack/IP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/IP.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/IP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/IP.o   ../TCPIP_Stack/IP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/IP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/IP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/MPFS2.o: ../TCPIP_Stack/MPFS2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/MPFS2.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/MPFS2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/MPFS2.o   ../TCPIP_Stack/MPFS2.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/MPFS2.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/MPFS2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/NBNS.o: ../TCPIP_Stack/NBNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/NBNS.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/NBNS.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/NBNS.o   ../TCPIP_Stack/NBNS.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/NBNS.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/NBNS.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/Reboot.o: ../TCPIP_Stack/Reboot.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/Reboot.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/Reboot.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/Reboot.o   ../TCPIP_Stack/Reboot.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/Reboot.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/Reboot.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/SPIRAM.o: ../TCPIP_Stack/SPIRAM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/SPIRAM.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/SPIRAM.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/SPIRAM.o   ../TCPIP_Stack/SPIRAM.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/SPIRAM.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/SPIRAM.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/StackTsk.o: ../TCPIP_Stack/StackTsk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/StackTsk.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/StackTsk.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/StackTsk.o   ../TCPIP_Stack/StackTsk.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/StackTsk.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/StackTsk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/TCP.o: ../TCPIP_Stack/TCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/TCP.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/TCP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/TCP.o   ../TCPIP_Stack/TCP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/TCP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/TCP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/Tick.o: ../TCPIP_Stack/Tick.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/Tick.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/Tick.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/Tick.o   ../TCPIP_Stack/Tick.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/Tick.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/Tick.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/UDP.o: ../TCPIP_Stack/UDP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/UDP.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/UDP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/UDP.o   ../TCPIP_Stack/UDP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/UDP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/UDP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/MainDemo.o: ../MainDemo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/MainDemo.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/MainDemo.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/MainDemo.o   ../MainDemo.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/MainDemo.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/MainDemo.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/SPIRAM2.o: ../SPIRAM2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/SPIRAM2.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/SPIRAM2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/SPIRAM2.o   ../SPIRAM2.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/SPIRAM2.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/SPIRAM2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/MP3Client.o: ../MP3Client.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/MP3Client.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/MP3Client.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/MP3Client.o   ../MP3Client.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/MP3Client.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/MP3Client.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/VLSICodec.o: ../VLSICodec.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/VLSICodec.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/VLSICodec.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/VLSICodec.o   ../VLSICodec.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/VLSICodec.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/VLSICodec.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/GenericTCPClient.o: ../GenericTCPClient.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/GenericTCPClient.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/GenericTCPClient.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/GenericTCPClient.o   ../GenericTCPClient.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/GenericTCPClient.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/GenericTCPClient.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/Command_Parser.o: ../Command_Parser.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Command_Parser.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Command_Parser.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/Command_Parser.o   ../Command_Parser.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/Command_Parser.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Command_Parser.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/Leds_Pwm.o: ../Leds_Pwm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Leds_Pwm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Leds_Pwm.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/Leds_Pwm.o   ../Leds_Pwm.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/Leds_Pwm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Leds_Pwm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/Dotacion_Movil.o: ../Dotacion_Movil.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Dotacion_Movil.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Dotacion_Movil.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/Dotacion_Movil.o   ../Dotacion_Movil.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/Dotacion_Movil.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Dotacion_Movil.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/Command_Execution.o: ../Command_Execution.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Command_Execution.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Command_Execution.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/Command_Execution.o   ../Command_Execution.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/Command_Execution.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Command_Execution.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/CustomHTTPApp.o: ../CustomHTTPApp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/CustomHTTPApp.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/CustomHTTPApp.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/CustomHTTPApp.o   ../CustomHTTPApp.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/CustomHTTPApp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/CustomHTTPApp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/IOConfiguration.o: ../IOConfiguration.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/IOConfiguration.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/IOConfiguration.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/IOConfiguration.o   ../IOConfiguration.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/IOConfiguration.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/IOConfiguration.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/InputHandler.o: ../InputHandler.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/InputHandler.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/InputHandler.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_ICD3=1 -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/InputHandler.o   ../InputHandler.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/InputHandler.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/InputHandler.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
else
${OBJECTDIR}/_ext/2146075967/EraseFlash.o: ../Flash/EraseFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2146075967" 
	@${RM} ${OBJECTDIR}/_ext/2146075967/EraseFlash.o.d 
	@${RM} ${OBJECTDIR}/_ext/2146075967/EraseFlash.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/2146075967/EraseFlash.o   ../Flash/EraseFlash.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2146075967/EraseFlash.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2146075967/EraseFlash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2146075967/ReadFlash.o: ../Flash/ReadFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2146075967" 
	@${RM} ${OBJECTDIR}/_ext/2146075967/ReadFlash.o.d 
	@${RM} ${OBJECTDIR}/_ext/2146075967/ReadFlash.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/2146075967/ReadFlash.o   ../Flash/ReadFlash.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2146075967/ReadFlash.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2146075967/ReadFlash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2146075967/WriteBlockFlash.o: ../Flash/WriteBlockFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2146075967" 
	@${RM} ${OBJECTDIR}/_ext/2146075967/WriteBlockFlash.o.d 
	@${RM} ${OBJECTDIR}/_ext/2146075967/WriteBlockFlash.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/2146075967/WriteBlockFlash.o   ../Flash/WriteBlockFlash.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2146075967/WriteBlockFlash.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2146075967/WriteBlockFlash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2146075967/WriteBytesFlash.o: ../Flash/WriteBytesFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2146075967" 
	@${RM} ${OBJECTDIR}/_ext/2146075967/WriteBytesFlash.o.d 
	@${RM} ${OBJECTDIR}/_ext/2146075967/WriteBytesFlash.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/2146075967/WriteBytesFlash.o   ../Flash/WriteBytesFlash.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2146075967/WriteBytesFlash.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2146075967/WriteBytesFlash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/2146075967/WriteWordFlash.o: ../Flash/WriteWordFlash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2146075967" 
	@${RM} ${OBJECTDIR}/_ext/2146075967/WriteWordFlash.o.d 
	@${RM} ${OBJECTDIR}/_ext/2146075967/WriteWordFlash.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/2146075967/WriteWordFlash.o   ../Flash/WriteWordFlash.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/2146075967/WriteWordFlash.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2146075967/WriteWordFlash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/ARP.o: ../TCPIP_Stack/ARP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/ARP.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/ARP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/ARP.o   ../TCPIP_Stack/ARP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/ARP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/ARP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/Announce.o: ../TCPIP_Stack/Announce.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/Announce.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/Announce.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/Announce.o   ../TCPIP_Stack/Announce.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/Announce.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/Announce.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/DHCP.o: ../TCPIP_Stack/DHCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/DHCP.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/DHCP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/DHCP.o   ../TCPIP_Stack/DHCP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/DHCP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/DHCP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/DNS.o: ../TCPIP_Stack/DNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/DNS.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/DNS.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/DNS.o   ../TCPIP_Stack/DNS.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/DNS.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/DNS.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/ETH97J60.o: ../TCPIP_Stack/ETH97J60.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/ETH97J60.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/ETH97J60.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/ETH97J60.o   ../TCPIP_Stack/ETH97J60.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/ETH97J60.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/ETH97J60.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/HTTP2.o: ../TCPIP_Stack/HTTP2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/HTTP2.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/HTTP2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/HTTP2.o   ../TCPIP_Stack/HTTP2.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/HTTP2.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/HTTP2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/Helpers.o: ../TCPIP_Stack/Helpers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/Helpers.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/Helpers.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/Helpers.o   ../TCPIP_Stack/Helpers.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/Helpers.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/Helpers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/ICMP.o: ../TCPIP_Stack/ICMP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/ICMP.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/ICMP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/ICMP.o   ../TCPIP_Stack/ICMP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/ICMP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/ICMP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/IP.o: ../TCPIP_Stack/IP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/IP.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/IP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/IP.o   ../TCPIP_Stack/IP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/IP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/IP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/MPFS2.o: ../TCPIP_Stack/MPFS2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/MPFS2.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/MPFS2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/MPFS2.o   ../TCPIP_Stack/MPFS2.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/MPFS2.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/MPFS2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/NBNS.o: ../TCPIP_Stack/NBNS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/NBNS.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/NBNS.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/NBNS.o   ../TCPIP_Stack/NBNS.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/NBNS.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/NBNS.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/Reboot.o: ../TCPIP_Stack/Reboot.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/Reboot.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/Reboot.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/Reboot.o   ../TCPIP_Stack/Reboot.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/Reboot.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/Reboot.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/SPIRAM.o: ../TCPIP_Stack/SPIRAM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/SPIRAM.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/SPIRAM.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/SPIRAM.o   ../TCPIP_Stack/SPIRAM.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/SPIRAM.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/SPIRAM.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/StackTsk.o: ../TCPIP_Stack/StackTsk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/StackTsk.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/StackTsk.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/StackTsk.o   ../TCPIP_Stack/StackTsk.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/StackTsk.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/StackTsk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/TCP.o: ../TCPIP_Stack/TCP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/TCP.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/TCP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/TCP.o   ../TCPIP_Stack/TCP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/TCP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/TCP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/Tick.o: ../TCPIP_Stack/Tick.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/Tick.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/Tick.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/Tick.o   ../TCPIP_Stack/Tick.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/Tick.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/Tick.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/927647166/UDP.o: ../TCPIP_Stack/UDP.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/927647166" 
	@${RM} ${OBJECTDIR}/_ext/927647166/UDP.o.d 
	@${RM} ${OBJECTDIR}/_ext/927647166/UDP.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/927647166/UDP.o   ../TCPIP_Stack/UDP.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/927647166/UDP.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/927647166/UDP.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/MainDemo.o: ../MainDemo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/MainDemo.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/MainDemo.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/MainDemo.o   ../MainDemo.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/MainDemo.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/MainDemo.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/SPIRAM2.o: ../SPIRAM2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/SPIRAM2.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/SPIRAM2.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/SPIRAM2.o   ../SPIRAM2.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/SPIRAM2.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/SPIRAM2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/MP3Client.o: ../MP3Client.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/MP3Client.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/MP3Client.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/MP3Client.o   ../MP3Client.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/MP3Client.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/MP3Client.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/VLSICodec.o: ../VLSICodec.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/VLSICodec.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/VLSICodec.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/VLSICodec.o   ../VLSICodec.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/VLSICodec.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/VLSICodec.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/GenericTCPClient.o: ../GenericTCPClient.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/GenericTCPClient.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/GenericTCPClient.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/GenericTCPClient.o   ../GenericTCPClient.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/GenericTCPClient.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/GenericTCPClient.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/Command_Parser.o: ../Command_Parser.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Command_Parser.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Command_Parser.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/Command_Parser.o   ../Command_Parser.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/Command_Parser.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Command_Parser.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/Leds_Pwm.o: ../Leds_Pwm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Leds_Pwm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Leds_Pwm.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/Leds_Pwm.o   ../Leds_Pwm.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/Leds_Pwm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Leds_Pwm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/Dotacion_Movil.o: ../Dotacion_Movil.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Dotacion_Movil.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Dotacion_Movil.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/Dotacion_Movil.o   ../Dotacion_Movil.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/Dotacion_Movil.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Dotacion_Movil.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/Command_Execution.o: ../Command_Execution.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/Command_Execution.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/Command_Execution.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/Command_Execution.o   ../Command_Execution.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/Command_Execution.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/Command_Execution.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/CustomHTTPApp.o: ../CustomHTTPApp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/CustomHTTPApp.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/CustomHTTPApp.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/CustomHTTPApp.o   ../CustomHTTPApp.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/CustomHTTPApp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/CustomHTTPApp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/IOConfiguration.o: ../IOConfiguration.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/IOConfiguration.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/IOConfiguration.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/IOConfiguration.o   ../IOConfiguration.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/IOConfiguration.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/IOConfiguration.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1472/InputHandler.o: ../InputHandler.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1472" 
	@${RM} ${OBJECTDIR}/_ext/1472/InputHandler.o.d 
	@${RM} ${OBJECTDIR}/_ext/1472/InputHandler.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -k -sco -DSEMAFORO_ID=5 -I".." -I"../Include/TCPIP_Stack" -I"../../../../../Program Files (x86)/Microchip/mplabc18/v3.47/h" -I"../Include" -I"." -I"../Flash/Include" -ml -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/_ext/1472/InputHandler.o   ../InputHandler.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1472/InputHandler.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1472/InputHandler.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/Internet_Radio_App.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE)   -p$(MP_PROCESSOR_OPTION_LD)  -w -x -u_DEBUG -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"  -z__MPLAB_BUILD=1  -u_CRUNTIME -z__MPLAB_DEBUG=1 -z__MPLAB_DEBUGGER_ICD3=1 $(MP_LINKER_DEBUG_OPTION) -l ${MP_CC_DIR}/../lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/Internet_Radio_App.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
else
dist/${CND_CONF}/${IMAGE_TYPE}/Internet_Radio_App.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE)   -p$(MP_PROCESSOR_OPTION_LD)  -w  -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"  -z__MPLAB_BUILD=1  -u_CRUNTIME -l ${MP_CC_DIR}/../lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/Internet_Radio_App.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/SEMAFORO_5
	${RM} -r dist/SEMAFORO_5

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
